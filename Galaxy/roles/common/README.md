#Ansible Role: Common

This Role install below list of packages for both RedHat and Debian based system. Below specified packages are help to monitor the server stats, configure nagios&pmm client and included some dependencies.

RedHat:
  - EPEL

RedHat and Debian:
----------------------------------------------------
    R                           |       D
----------------------------------------------------
  - sysstat                     |   - sysstat
  - nethogs                     |   - nethogs
  - hdparm                      |   - hdparm
  - screen                      |   - screen
  - nc                          |   - netcat
  - bc                          |   - bc
  - innotop                     |   - iftop
  - iftop                       |   - net-tools
  - net-tools                   |   - iotop
  - innotop                     |   - python-pip
  - perf                        |   - percona-toolkit
  - telnet                      |   - pmm-client
  - python2-pip                 |   - percona-nagios-plugins
  - percona-toolkit             |   - libdbi-perl
  - pmm-client                  |   - libnagios-plugin-perl
  - percona-nagios-plugins.noarch|  - git
  - perl-DBI.x86_64             |
  - perl-Nagios-Plugin.noarch   |
  - git                         |
------------------------------------------------------------------


Requirements
------------
None.

Role Variables
--------------
Available variables are listed below, along with default values (see defaults/main.yml):

epel_repo_url: "https://dl.fedoraproject.org/pub/epel/epel-release-latest-{{ ansible_distribution_major_version }}.noarch.rpm"

epel_repo_gpg_key_url: "/etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-{{ ansible_distribution_major_version }}"

percona_repo_url: "https://www.percona.com/redir/downloads/percona-release/redhat/percona-release-0.1-4.noarch.rpm"

percona_deb_url: "https://repo.percona.com/apt/percona-release_0.1-4.{{ ansible_distribution_release }}_all.deb"

The EPEL repo URL, GPG key URL, percona repo url and percona deb url. Generally, these should not be changed, but if this role is out of date, or if you need a very specific version, these can both be overridden.


Dependencies
------------
None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - common

License
-------

MIT/BSD

Author Information
------------------

This Role was created in 2017 by Manosh Malai.
