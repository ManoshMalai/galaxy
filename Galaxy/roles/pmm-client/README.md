PMM-Client
=========

An Ansible Role that configure the pmm client to collect metric from Mysql, Mongo, Proxysql, AWS RDS and Aurora Cluster server and send that to user defined PMM Server.


Requirements
------------
1. This Ansible role configure any PMM Client service metric according to the group_name we defined on the hosts file and <playbook>.yml file. Below I have shared few example for better understand.

    e.g.
      1. 
        [pmmclientmysql]------> This is group_name
        centos ansible_host=192.168.33.11
        ubuntu ansible_host=192.168.33.12
      2. 
        [pmmclientaurora]------> This is group_name
        centos ansible_host=192.168.33.11
        ubuntu ansible_host=192.168.33.12
      3.
      [pmmclientlinux]

      4.
      [pmmclientmongo]

      5.
      [pmmclientproxysql]

      6.
      [pmmclientaurora]

      7.
      [pmmclientawsrds]

2. This Ansible Roles depends on lot of variable. So please
go through the full README before start execute the role. Below I have shared full details of all the variable, kindly go through it.

Role Variables
--------------
Available variables are listed below, along with default values (see `defaults/main.yml`):

  pmm_server_ip: "" #Its Null
  pmm_server_port: "8080"

We use this variable on "Connecting pmm Client to PMM Server" task. This variable represent the PMM server ip and its service port number.

  pmm_server_webuser: "pmmadmin"
  pmm_server_webpass: "MydbopsAdmin@17"
 
We use this variable on "Connecting pmm Client to PMM Server" task. This variable provides the default PMM server authentication details, Suppose user have different username and credentials they can define the same on hosts and playbook on "vars" section.

 Important Note:
              server_user: "pmmadmin"
              server_password: "MydbopsAdmin@17"
              
When user use pmm-server role to create and run PMM Server Container, they have options to define server_user and server_password. suppose they didn't define, playbook will use the above defined default username and credentials and configure the PMM Server container. So that in this role pmm_server_webuser and pmm_server_webpass same default password what we defined on the pmm-server role.

  pmm_default_cuser: ""
  pmm_default_cpass: ""

This variable represent Client mysql user who has privilege to collect metric data from that mysql server. We use the above variable on all "Enable PMM Mongo/Mysql/Proxysql Metrics task's". By default this variable will be empty. so when user create their hosts or playbook file, they need to define this detail's.

  pmm_default_mongo_port: 27017

We use this variable to define custom mongo port. if client mongo server using default, we can left this empty.

  rds_server:
  - name:  #identifier
    hostname: #ip or rds instance name
  - name: #identifier
    hostname: #ip or rds instance name

We use the above variable to configure the AWS RDS and Aurora cluster's. In this case we can't directly install pmm client on RDS or Aurora, So we spin ec2 instance on same zone and install PMM client on the same. Then we configure the PMM client to listen and retrieve metrics data from those RDS and Aurora instance.

We need to define Unique business purpose name on "name:", this name only appear on the Grafana UI. Then every name should be unique. 

  pmm_default_rds_user: ""
  pmm_default_rds_pass: ""

We use the above variable to configure the AWS RDS and Aurora cluster's. This credential details help to auth with RDS and Aurora cluster and collect Metrics data from the same. By default it's empty.

  aws_secret_access_key: ""
  aws_access_key_id: ""

This variable's help to collect linux metric from cloudwatch. The scope of this variable only for "Enable Cloud watch for AWS RDS and Aurora Linux Monitoring". By default it will be empty.


Dependencies
------------
Common



Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - pmm-client

License
-------

MIT/BSD

Author Information
------------------

This Role was created in 2017 by Manosh Malai.
