Role Name
=========

This role pull pmm docker image, create volume for the pmm container and run the docker container with some environment.

Requirements
------------

None.

Role Variables
--------------
Available variables are listed below, along with default values (see `defaults/main.yml`):

server_user: "pmmadmin"
The above specified variable will be used when we execute the docker run command, then this username will be used to login into PMM Login page. The above specified will be the default value, user can change the variable value while call playbook.

server_password: "MydbopsAdmin@17"
The above specified variable will be used when we execute the docker run command, then this password will be used to login into PMM Login page. The above specified will be the default value, user can change the variable value while call playbook.

http_forward_port: 8080
This docker container run http service for pmm, it will run on default port 80. Using port forward concept only we can access the service outside of the container. By default we forward 80 to 8080, but user can customize the port using this variable.

Dependencies
------------
common
docker



Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - common
         - docker
         - pmm-server

License
-------

MIT/BSD

Author Information
------------------

This Role was created in 2017 by Manosh Malai.
